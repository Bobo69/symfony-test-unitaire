<?php

namespace App\Tests\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Calculus;


class CalculusTest extends TestCase{
  private $calculus;
  
  public function setUp() {
    $this->calculus = new Calculus;
  }
  
  // public function testResultSuccess() {
  //   $result = $this->calculus->result(1,1,"+");
  //   $expect =2;

  /**
   * @dataProvider successProvider
   */

   public function testResultSuccess($a, $b, $operator, $expect) {
    $result = $this->calculus->result($a, $b, $operator);
    
  

    $this->assertEquals($result, $expect);
  }
  public function successProvider() {
    return [
      [1,1,"+", 2],
      [1,1,"-", 0],
      [1,1,"*", 1],
      [4,2,"/", 2],
      
    ];
  }
}

