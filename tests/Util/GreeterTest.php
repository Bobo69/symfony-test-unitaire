<?php

namespace App\Tests\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Greeter;


class GreeterTest extends TestCase {
  private $greeter; //pour metter la ligne 13 dedans

  public function setUp() { //mettre en place lenvirronemet de test
    $this->greeter = new Greeter();
  }

  public function testFirst() {
    $this->assertEquals("bloup", "bloup");
    $this->assertFalse(false);
  }
  public function testGreetSuccess() {
    //$greeter = new Greeter();
    $result = $this->greeter->greet("bloup");         //declenche ca methode greet et ont recupere le resultat de la ethode de
                                                //ans la var result
    $except = "Hello bloup, how's it going ?";
    $this->assertEquals($except, $result);
  }
  public function testGreetNumber() {
    //$greeter = new Greeter();
    $result = $this->greeter->greet(45);
    $except = "Hello 45, how's it going ?";
    $this->assertEquals($except, $result);
  }
}