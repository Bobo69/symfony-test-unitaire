<?php

namespace App\Util;

class Calculus {
  public function result($a, $b, $operator) {
    switch ($operator) {
      case '+':
        return $a+$b;
      case '-':
        return $a-$b;
      case '*':
          return $a*$b;
      case '/':
        return $a/$b;
    }
  }
}